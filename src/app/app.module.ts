import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {HttpModule, JsonpModule} from '@angular/http';

import { AppComponent } from './app.component';
import { WeatherComponent } from './weather-widget/components';
import {SpeedUnitPipe, TemperatureUnitPipe} from "./weather-widget/pipe";

@NgModule({
  declarations: [
    AppComponent,
    WeatherComponent,
    SpeedUnitPipe,
    TemperatureUnitPipe
  ],
  imports: [
    BrowserModule,
    HttpModule,
    JsonpModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
