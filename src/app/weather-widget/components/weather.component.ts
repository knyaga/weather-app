import {Component, OnInit} from '@angular/core';
import {WeatherService} from "../service/weather.service";
import {Weather} from "../model/weather.model";

declare var Skycons: any;

@Component({
  moduleId: module.id,
  selector: "weather-widget",
  templateUrl:"weather.component.html",
  styleUrls: ['weather.component.css'],
  providers: [WeatherService]
})
export class WeatherComponent implements OnInit{
  position: Position;
  weather = new Weather(null, null, null, null, null);
  currentSpeedUnit: string = "kph";
  currentTempUnit: string = "celsius";
  currentLocationName: string = "";
  weatherDataReceived = false;
  icons = new Skycons({ color: "#FFF"});

  constructor(private weatherService: WeatherService){}

  ngOnInit(): void {
    this.getCurrentLocation()
  }

  getCurrentLocation() {
    this.weatherService.getCurrentLocation()
      .subscribe(position => {
        this.position = position,
          this.getCurrentWeather();
          this.getLocationName();
      }, error => {
        console.error(error);
      });
  }

  getCurrentWeather() {
    this.weatherService.getCurrentWeather(this.position.coords.latitude, this.position.coords.longitude)
        .subscribe(weather => {
          this.weather.temperature = weather.currently.temperature;
          this.weather.summary     = weather.currently.summary;
          this.weather.wind        = weather.currently.windSpeed;
          this.weather.humidity    = weather.currently.humidity;
          this.weather.icon        = weather.currently.icon;
          this.setIcon();
          console.log(this.weather); // TODO: remove
          this.weatherDataReceived = true;

        }, error => {
          console.log(error);
        });
  }

  getLocationName() {
    this.weatherService.getLocationName(this.position.coords.latitude, this.position.coords.longitude)
       .subscribe(location => {
         console.log(location);
         this.currentLocationName = location["results"][4]["formatted_address"];
       }, error => {
         console.error(error);
       });
  }

  toggleUnits() {
    this.toggleTempUnits();
    this.toggleSpeedUnits();
  }

  toggleTempUnits() {
    if(this.currentTempUnit == "celsius") {
      this.currentTempUnit = "fahrenheit";
    } else {
      this.currentTempUnit = "celsius";
    }
  }

  toggleSpeedUnits() {
    if(this.currentSpeedUnit == "kph") {
      this.currentSpeedUnit = "mph";
    } else {
      this.currentSpeedUnit = "kph";
    }
  }

  setIcon() {
    this.icons.add("icon", this.weather.icon);
    this.icons.play();
  }
}
