/**
 * Created by Otis on 20/01/17.
 */
import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'tempUnit'
})
export class TemperatureUnitPipe implements PipeTransform {
  transform(temp: number, unitType: string) {
    switch (unitType) {
      case "celsius":
        const celsius = (temp - 32) * 0.5556;
        return celsius;
      default:
        return temp;
    }
  }
}
