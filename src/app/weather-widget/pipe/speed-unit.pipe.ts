/**
 * Created by Otis on 20/01/17.
 */
import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'speedUnit'
})
export class SpeedUnitPipe implements PipeTransform {
  transform(windSpeed: number, unitType: string) {
    switch (unitType) {
      case "mph":
        const speedInMiles = Number(windSpeed * 1.6).toFixed(0);
        return speedInMiles + "mph";
      default:
        return Number(windSpeed).toFixed(0) + "kph";
    }
  }

}
