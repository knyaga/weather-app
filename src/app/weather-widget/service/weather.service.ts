/**
 * Created by Otis on 19/01/17.
 */
import {Injectable} from '@angular/core';
import {Observable} from 'rxjs/Observable';
import {Jsonp, Http} from "@angular/http";

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

import {FORECAST_API_KEY, FORECAST_API_URL,
  GEO_CODE_URL, GEO_CODE_API_KEY
} from '../constants/constants';


@Injectable()
export class WeatherService {
  constructor(private jsonp: Jsonp, private http: Http){}

  getCurrentLocation() : Observable<any> {
    if(navigator.geolocation) {
      return Observable.create(observer => {
        navigator.geolocation.getCurrentPosition(coordinates => {
          observer.next(coordinates);
        }),error => {
          return Observable.throw(error);
        }
      });
    } else {
      return Observable.throw("Geo-location not available");
    }
  }

  getCurrentWeather(lat:number, long:number): Observable<any> {
    const url = `${FORECAST_API_URL}${FORECAST_API_KEY}/${lat},${long}?callback=JSONP_CALLBACK`;

    return this.jsonp.get(url)
               .map(data => data.json())
               .catch(error => {
                 return Observable.throw(error.json())
               });
  }

  getLocationName(lat: number, lng: number): Observable<any> {
    const url = `${GEO_CODE_URL}?latlng=${lat},${lng}&key=${GEO_CODE_API_KEY}`;

    return this.http.get(url)
               .map(location => location.json())
               .catch(error => {
                 return Observable.throw("No location name found: " + error);
               });

  }

}
